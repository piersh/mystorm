#ifndef CFG_H
#define CFG_H

#define MAX_TICK_TASKS ((uint8_t) 10)


#define NVIC_SYS_TICK_PREEMPTION_PRIORITY       ((uint8_t)0)
#define NVIC_SYS_TICK_SUB_PRIORITY                      ((uint8_t)0)
#define SYS_TICK_FREQUENCY                                        ((uint32_t) 10000)

#endif // CFG_H
